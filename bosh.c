/*
bosh main file

$Id: bosh.c,v 1.106 2009/03/26 14:31:01 alexsisson Exp $

(C) Copyright 2004-2009 Alex Sisson (alexsisson@gmail.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <ncurses.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <sys/wait.h>
#include <errno.h>
#include <time.h>
#include <sys/types.h>

#include "bosh.h"
#include "list.h"
#include "rc.h"
#include "system.h"
#include "misc.h"
#include "stray.h"

/* defines */
#define HEADER 2
#define FOOTER 2
#define FRAME  (HEADER+FOOTER)

/* globals */
char    *REPLY = NULL;      /* input return */
int      ULINES;            /* usable lines */
char    *conf = NULL;
char    *confpath = NULL;
bosh_t  *bosh = NULL;       /* pointer to current BOSH struct */
LIST    *boshlist = NULL;   /* linked list for BOSH structs */
int      boshlistpos = 0;
MEVENT  *mouse = NULL;      /* ncurses mouse struct - unused at the moment */
char    *search = NULL;     /* previous search */
time_t   autorefreshtime;
char    *shell = "/bin/bash";

char    *BOSH = NULL;
char    *BOSHPARAM = NULL;
char     BOSHVARFILE[18];
char    *BOSHERR = NULL;
int      boshuservars;
char   **boshuservar;

stray_t *mainarg = NULL;
stray_t *childarg = NULL;


/* functions */

/* alloc and init bosh_t structure */
bosh_t *bosh_t_new() {
  bosh_t *r;
  r = malloc(sizeof(bosh_t));
  if(r) {
    memset(r,0,sizeof(bosh_t));
    r->exit = BOSH_EXIT_UNKNOWN;
    r->childin = -1;
    r->childout = -1;
    r->childerr = -1;
    r->width = BOSH_WIDTH_DEFAULT;
    r->cursorsize = 1;
    r->cursormovement = 1;
    r->buf = malloc(sizeof(LIST));
    list_init(r->buf);
    r->searchwrap = 1;
  }
  return r;
}

/* frees a bosh_t structure */
int bosh_t_free(bosh_t *b) {
  int i;
  free(b->command);
  free(b->pipe);
  free(b->preaction);
  if(b->multilineseperator)
    if(strlen(b->multilineseperator))
      free(b->multilineseperator);
  for(i=0;i<37;i++)
    free(b->action[i].command);
  close(b->childin);
  close(b->childout);
  close(b->childerr);
  if(b->childpid)
    kill(b->childpid,9);
  list_free(b->buf);
  free(b);
  return 0;
}

/* bosh_init: init anything that needs to be re-inited by key F6  */
void bosh_init() {
  int i;

  if(boshlist) //TODO: free properly
    free(boshlist);

  boshlist = malloc(sizeof(LIST));
  list_init(boshlist);
  bosh = bosh_t_new();
  list_add(boshlist,bosh);

  if(boshuservars) {
    for(i=0;i<boshuservars;i++)
      free(boshuservar[i]);
    free(boshuservar);
  }

  bosh_parseargs(bosh,mainarg);

  if(boshuservars) {
    boshuservar = malloc(boshuservars*sizeof(char*));
    memset(boshuservar,0,boshuservars*sizeof(char*));
  }
}

/* finish ncurses and display BOSHERR if set */
void bosh_finish(int s) {
  if(s==SIGSEGV)
    BOSHERR = "received SIGSEGV :-(";
  mvaddstr(LINES-1,0,strmak(COLS,' '));
  refresh();
  endwin();
  bosh_unlink(BOSHVARFILE);
  bosh_unlink(bosh->tmpfscript);
  bosh_unlink(bosh->tmpfpipe);
#ifdef LOG
  bosh_log("sig: %d - exiting (%s)\n",s,BOSHERR?BOSHERR:"");
#endif
  if(BOSHERR)
    fprintf(stderr,"%s: %s\n",PACKAGE,BOSHERR);
  exit(s);
}

/* ctrlc handler */
void ctrlc(int s) {
  ungetch(3);
}

int keyup(int n) {
  if(!n)
    n = bosh->cursormovement;
  if(bosh->cursor >= n)
    bosh->cursor -= n;
  else if(bosh->offset >= n)
    bosh->offset -= n;
  if(bosh->offset==0 && bosh->cursor < bosh->header)
    bosh->cursor = bosh->header;
  bosh->line = bosh->offset + bosh->cursor;
  return n;
}

int keydown(int n) {
  if(!n)
    n = bosh->cursormovement;
  if(bosh->line+bosh->cursormovement>=bosh->buf->items)
    return 0;
  if(bosh->cursor + n < ULINES)
    bosh->cursor += n;
  else
    bosh->offset += n;
  bosh->line = bosh->offset + bosh->cursor;
  return n;
}


static int *intalloc(int n) {
  int *r = malloc(sizeof(int));
  if(r)
    memcpy(r,&n,sizeof(int));
  return r;
}



/*
 * NORMAL MODE KEY HANDLER
 *
 */
int *keyhandler_normal(int key) {
  int n;
  switch(key) {
    case KEY_UP:    keyup(0);                    break;
    case KEY_DOWN:  keydown(0);                  break;
    case KEY_PPAGE: for(n=0;n<8;n++) keyup(0);   break;
    case KEY_NPAGE: for(n=0;n<8;n++) keydown(0); break;

    case KEY_LEFT:
      if(boshlistpos) {
        boshlistpos--;
        bosh = list_get(boshlist,boshlistpos);
        if(bosh->refresh)
          bosh_open(bosh);
      }
      break;

    case KEY_RIGHT:
      if(boshlistpos+1<boshlist->items) {
        boshlistpos++;
        bosh = list_get(boshlist,boshlistpos);
        if(bosh->refresh)
          bosh_open(bosh);
      }
      break;
    case KEY_TAB:
      break;
    case KEY_SPACE:
      break;

    case KEY_CTRLA:
//      readstr("action");
//      if(REPLY) {
//              bosh->action[
//      }
      break;
/*
old save code
            readstr("file");
            if(REPLY) {
              rc_write(REPLY,bosh);
              free(conf);
              conf = strdup(REPLY);
            }
            break;
*/

    /* ^J: jump to line */
    case KEY_CTRLJ:
      readstr("line");
      if(REPLY) {
        n = strtol(REPLY,0,10);
        if(n>0) {
          n--;
          if(n<bosh->line)
            while(n<bosh->line)
              keyup(1);
          else
            while(n>bosh->line && keydown(1));
        }
      }
      break;

    /* ^K: kill child - not working */
    case KEY_CTRLK:
      if(bosh->childpid) {
        n = kill(bosh->childpid,BOSH_KILL_SIGNAL);
#ifdef LOG
        bosh_log("kill: %d returned: %d, %s\n",bosh->childpid,n,n?errstr:"");
      } else {
        bosh_log("kill: no child\n");
#endif
      }
      break;

    /* ^O: run new command */
    case KEY_CTRLO:
      readstr("command");
      if(REPLY) {
        free(bosh->command);
        bosh->command = strdup(REPLY);
        ungetch(KEY_CTRLR);
      }
      break;

    /* ^P,|: pipe buffer through command */
    case KEY_CTRLP:
    case '|':
      readstrp("pipe",bosh->pipe);
      if(REPLY) {
        if(strlen(REPLY)) {
          free(bosh->pipe);
          bosh->pipe = strdup(REPLY);
        }
        if(bosh->pipe) {
          if(bosh_pipe(bosh,bosh->pipe)==0) {
            char *p;
            if(bosh->command) {
              p = malloc(strlen(bosh->command)+strlen(bosh->pipe) + 4);
              sprintf(p,"%s | %s",bosh->command,bosh->pipe);
              free(bosh->command);
            } else {
              p = malloc(strlen(bosh->pipe) + 3);
              sprintf(p,"| %s",bosh->pipe);
            }
            bosh->command = p;
          }
        }
      }
      break;

    /* ^R: refresh */
    case KEY_CTRLR:
    case KEY_F(5):
      if(conf) {
#ifdef LOG
        bosh_log("bosh: refresh: running main command\n");
#endif
        bosh_open(bosh);
      }
      break;

    /* ^T: "thru" mode */
    //case KEY_CTRLT:
    //  bosh_mainloop(BOSH_MODE_THRU);
    //  break;

    /* ^W: search */
    case KEY_F(3):
    case KEY_CTRLW:
    case '/':
      readstrp("search",search);
      if(!REPLY)
        break;
      if(strlen(REPLY)) {
        bosh_regex_init(REPLY);
        free(search);
        search = strdup(REPLY);
#ifdef LOG
        bosh_log("search: new search \"%s\"\n",search);
#endif
      }
      /* drop thru */
    case KEY_F(4):
    case KEY_CTRLN:
      if(search) {
        char *p;
        n = bosh->line; /* starting line */
#ifdef LOG
        bosh_log("search: starting search \"%s\"\n",search);
#endif
        keydown(1);
        while(1) {
          if(n==bosh->line)
            break;
          p = list_get(bosh->buf,bosh->line);
          if(!p)
            break;
#ifdef LOG
          bosh_log("search: current line %d: \"%s\"\n",bosh->line,p);
#endif
          if(!bosh_regex_try(p)) /* match */
            break;
          if(bosh->line+1>=bosh->buf->items) {
            /* end of list */
            if(bosh->searchwrap) {
              bosh->line = bosh->offset = bosh->cursor = 0;
              continue;
            }
            else
              break;
          }
          if(!keydown(1))
            break;
        }
      }
      break;

    /* ^V: view conf */
    case KEY_CTRLV:
      bosh_action(bosh,BOSH_ACTION_SPECIAL_SHOWCONF);
      break;

    /* ^X: exit */
    case KEY_CTRLX:
      bosh_finish(0);

/*
    case KEY_MOUSE:
      getmouse(mouse);
      if(mouse->x > 2-2 && mouse->x < 2+2)
      ungetch(keyyuowant)
      break;
*/

    /* F6: reload config */
    case KEY_F(6):
      if(conf) {
        bosh_init(bosh);
        if(bosh->command) {
#ifdef LOG
          bosh_log("bosh: running main command\n");
#endif
          bosh_open(bosh);
          if(bosh->width<strlen(bosh->command))
            bosh->width = strlen(bosh->command);
        }
      }
      break;

    /* F12/^L redraw and resize */
    case KEY_CTRLL:
    case KEY_F(12):
      erase();
      /* drop thru */
    case KEY_RESIZE:
      refresh();
      break;

    /* action */
    default:
      n = isdigit(key) ? key - 48 : isupper(key) ? key - 55 : islower(key) ? key - 87 : key=='\n' ? 36 : -1;
      if(n>=0)
        if(bosh->action[n].command) {
#ifdef LOG
          bosh_log("action: %c\n",toupper(key));
#endif
          if(bosh->action[n].prompt) {
            readstr(bosh->action[n].prompt);
            if(REPLY)
              BOSHPARAM = strdup(REPLY);
          }
          bosh_action(bosh,n);
        }
      break;
  }
  return NULL;
}


int *keyhandler_readstr(int key) {
  switch(key) {
    case KEY_CTRLX:
      bosh_finish(0);
    case KEY_CTRLC:
      return intalloc(-1);
    case KEY_CTRLH:
    case KEY_BKSPC:
    case KEY_BACKSPACE:
      if(strlen(REPLY)) {
        REPLY[strlen(REPLY)-1] = 0;
        mvaddch(stdscr->_cury,stdscr->_curx-1,' ');
        move(stdscr->_cury,stdscr->_curx-1);
      }
      break;
    case '\n':
      return intalloc(0);
    default:
      if(isprint(key)) {
        REPLY[strlen(REPLY)] = key;
        addch(key);
        refresh();
      }
      break;
  }
  return NULL;
}


int *keyhandler_thru(int key) {
  switch(key) {
    case KEY_CTRLT:
      return intalloc(0);
    default:
      bosh_write(bosh,key);
  }
  return NULL;
}



/* * * * * * * * * * * * * * * * * * * * *
 *
 * M A I N !!!
 *
 * * * * * * * * * * * * * * * * * * * * */
int main(int argc, char **argv) {

#ifdef LOG
  bosh_log_open("w");
#endif

  mainarg = stray_new_d(argc,argv);

  bosh_init();

  strcpy(BOSHVARFILE,"/tmp/boshv_XXXXXX");
  close(mkstemp(BOSHVARFILE));

  if(bosh->command) {
#ifdef LOG
    bosh_log("bosh: running main command\n");
#endif
    bosh_open(bosh);
    if(bosh->width<strlen(bosh->command))
      bosh->width = strlen(bosh->command);
  }

  if(!isatty(0)) {
    /* piped input */
    bosh->childout = dup(0);
    dup2(1,0);
    bosh->command = NULL;
  } else if(!bosh->command) {
    fprintf(stderr,"%s: no input\n",PACKAGE);
    return 1;
  }

  /* set up signal handlers */
  signal(SIGTERM,bosh_finish);
  signal(SIGHUP, bosh_finish);
  signal(SIGSEGV,bosh_finish);
  signal(SIGINT, ctrlc);
  signal(SIGPIPE,SIG_IGN);

  /* init ncurses */
  initscr();
  keypad(stdscr,TRUE);
  cbreak();
  noecho();
  halfdelay(2);
  if(mouse)
    mousemask(BUTTON1_PRESSED,NULL);

  if(bosh->autorefresh>0)
    autorefreshtime = time(NULL);

  /* sets cursor position correctly if header option is used */
  keyup(0);

  /* start main loop */
  bosh_mainloop(BOSH_MODE_NORMAL);
  return 0;
}



//#define debug(s) list_adddup(bosh->buf,s,strlen(s)+1)



/* * * * * * * * * * * * * * * * * * * * *
 *
 * bosh_mainloop
 *
 * * * * * * * * * * * * * * * * * * * * */
int bosh_mainloop(int mode) {
  int key;
  int n = 0;
  int needredraw;
  int *r = NULL;

  while(1) {
    needredraw = 0;
/*
    if the cursor is currently above the buffer length, it suggests that the input has
    been refreshed, so we wait a little while so that so data can arrive. This way, the
    refresh code won't just "correct" the problem, and lose cursor position.
*/
    bosh = list_get(boshlist,boshlistpos);
    if(bosh->line>bosh->buf->items)
      usleep(BOSH_INPUTWAIT_USLEEP);
    if(bosh->autorefresh>0) {
      if(time(NULL)>=autorefreshtime+bosh->autorefresh) {
        bosh_open(bosh);
        autorefreshtime = time(NULL);
      }
    }

    needredraw = bosh_read(bosh);

    key = getch();
    // This breaks KEY_RESIZE handling ??????????????
    // if(key==ERR && !needredraw)
    //   continue;

#ifdef LOG
    if(key!=ERR) {
      char *k = malloc(4);
      if(isprint(key)) {
        k[0] = key;
        k[1] = 0;
      } else if islower(key+96) {
        k[0] = '^';
        k[1] = key + 96;
        k[2] = 0;
      } else {
        k[0] = ' ';
        k[1] = 0;
      }
      bosh_log("key: %03d '%s' (mode:%d)\n",key,k,mode);
    }
#endif

    switch(mode) {
      case BOSH_MODE_NORMAL:
        r = keyhandler_normal(key);
        bosh_redraw();
        break;
      case BOSH_MODE_READSTR:
        r = keyhandler_readstr(key);
        break;
      case BOSH_MODE_EDIT:  /* not supported yet */
        switch(key) {
          /* insert blank line into list */
          case KEY_CTRLU:
          case KEY_IC:
            list_insdup(bosh->buf,bosh->line,"\0",1);
            break;
          /* delete item from list */
          case KEY_CTRLK:
          case KEY_DC:
            list_del(bosh->buf,bosh->line);
            if(bosh->line>=bosh->buf->items)
              ungetch(KEY_UP);
            break;
        }
        break;
      case BOSH_MODE_THRU:
        r = keyhandler_thru(key);
        break;
    }
    if(r) {
      n = *r;
      free(r);
      return n;
    }
  }
}




/*
 * bosh_redraw
 *
 */
#define WIDTHOF(n) ((n<10?1:(n<100?2:(n<1000?3:(n<10000?5:6)))))
int bosh_redraw() {
  int i;
  char *p;
  ULINES = LINES - FRAME;
  erase();
//  Ensure cursor is in buffer range. Commented out because it was being called on a refresh
//  while(bosh->offset+bosh->cursor>list_items(bosh->buf)+1)
//    keyup();
  attron(A_REVERSE);
  mvaddstr(0,0,strmak(COLS,' '));
  if(conf)
    mvprintw(0,0,"%s",conf);
  if(bosh->title)
    printw(" (%s)",bosh->title);
  mvprintw(0,COLS-(strlen(PACKAGE)+strlen(VERSION)+1),"%s %s",PACKAGE,VERSION);
  attroff(A_REVERSE);
  list_start(bosh->buf,bosh->offset);
  for(i=0;i<ULINES;i++) {
    p = list_next(bosh->buf);
    if(p) {
      if(i>=bosh->cursor && i<bosh->cursor+bosh->cursorsize)
        attron(A_REVERSE);
      mvaddstr(HEADER+i,0,strmak(lof(COLS,bosh->width),' '));
      mvaddstr(HEADER+i,0,p);
      //  this makes bosh crash :-( - linked list could be dodgy:   addstr(strmak(lof(COLS,bosh->width)-strlen(p),' '));
      if(i>=bosh->cursor && i<bosh->cursor+bosh->cursorsize)
        attroff(A_REVERSE);
    } else {
      mvaddstr(HEADER+i,0,strmak(lof(COLS,bosh->width),' '));
    }
  }

  mvaddstr(LINES-2,0,strmak(COLS,' '));

  /* bottom line */
  attron(A_REVERSE);
//  mvaddstr(LINES-2,0,strmak(lof(COLS,bosh->width),' '));
//  mvprintw(LINES-2,0,"%s",bosh->command?bosh->command:"[stdin]");
  mvaddstr(LINES-1,0,strmak(lof(COLS,bosh->width),' '));

  i = bosh->buf->items;
  if(bosh->cursorsize>1)
    mvprintw(LINES-1,0,"%*d-%*d/%d ",WIDTHOF(i),bosh->line+1,WIDTHOF(i),lof(bosh->line+1+bosh->cursorsize-1,i),i);
  else
    mvprintw(LINES-1,0,"%0*d/%d ",WIDTHOF(i),bosh->line,i);

  if(bosh->exit>BOSH_EXIT_UNKNOWN) {
    printw(" exit=");
    if(bosh->exit>=0)
      printw("%d",bosh->exit);
    else
      printw("sig:%d",bosh->exit*-1);
  }

  printw(" %s ",bosh->refresh?"R":" ");
  if(bosh->autorefresh)
    printw("%d",bosh->autorefresh-(time(NULL)-autorefreshtime));
  attroff(A_REVERSE);
  move(LINES-1,COLS-1);

  refresh();
  return 0;
}

int readstr(char *prompt) {
  int n;
  free(REPLY);
  REPLY = malloc(128);
  if(!REPLY)
    return -1;
  memset(REPLY,0,128);
  attron(A_REVERSE);
  mvaddstr(LINES-1,0,strmak(bosh->width,' '));
  mvprintw(LINES-1,0,"%s: ",prompt);
  n = bosh_mainloop(BOSH_MODE_READSTR);
  attroff(A_REVERSE);
  if(n) {
    /* cancelled */
    free(REPLY);
    REPLY = NULL;
    return -1;
  }
  return 0;
}

int readstrp(char *prompt, char *previous) {
  char *s;
  int r;
  if(previous) {
    s = malloc(strlen(prompt)+strlen(previous)+4);
    sprintf(s,"%s [%s]",prompt,previous);
    r = readstr(s);
    free(s);
  } else {
    r = readstr(prompt);
  }
  return r;
}
