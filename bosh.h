/*
$Id: bosh.h,v 1.58 2009/03/26 17:07:51 alexsisson Exp $

(C) Copyright 2004-2009 Alex Sisson (alexsisson@gmail.com)

This file is part of bosh.

bosh is free software; you can redistribute it and/or modify
under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

bosh is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with bosh; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/


#ifndef BOSH_H_INCLUDED
#define BOSH_H_INCLUDED

#include <stdio.h>
#include <sys/types.h>

#include "list.h"
#include "stray.h"

#define BOSH_WIDTH_DEFAULT            32
#define BOSH_PIPE_USLEEP            1000
#define BOSH_INPUTWAIT_USLEEP     100000

/* bosh interface modes */
#define BOSH_MODE_NORMAL   0
#define BOSH_MODE_READSTR  1
#define BOSH_MODE_EDIT     2
#define BOSH_MODE_THRU     3

/* bosh action types */
#define BOSH_ACTION_COMMAND  0x01
#define BOSH_ACTION_BOSH     0x02

/* bosh action output destinations */
#define BOSH_ACTION_OUTPUT_NONE       ' '
#define BOSH_ACTION_OUTPUT_OVERWRITE  '.'
#define BOSH_ACTION_OUTPUT_ADVANCE    '>'
#define BOSH_ACTION_OUTPUT_ENDCURSES  '!'
#define BOSH_ACTION_OUTPUT_NEWWINDOW  /* not decided  */

/* bosh special actions - must be greater than 37 */
#define BOSH_ACTION_SPECIAL_SHOWCONF  40

/* kill signal to send to children */
#define BOSH_KILL_SIGNAL SIGTERM

/* special values for bosh_t.exit */
#define BOSH_EXIT_UNKNOWN -100

/* meaning of bosh_popen cmd argument */
#define BOSH_POPEN_ACTION             1
#define BOSH_POPEN_NODUP              2
#define BOSH_POPEN_WAIT               4
#define BOSH_POPEN_FILTER             16

/* ncurses-style key defitions */
#define KEY_CTRLA       1
#define KEY_CTRLB       2
#define KEY_CTRLC       3
#define KEY_CTRLD       4
#define KEY_CTRLE       5
#define KEY_CTRLF       6
#define KEY_CTRLG       7
#define KEY_CTRLH       8
#define KEY_CTRLI       9
#define KEY_CTRLJ       10
#define KEY_CTRLK       11
#define KEY_CTRLL       12
#define KEY_CTRLM       13
#define KEY_CTRLN       14
#define KEY_CTRLO       15
#define KEY_CTRLP       16
#define KEY_CTRLQ       17
#define KEY_CTRLR       18
#define KEY_CTRLS       19
#define KEY_CTRLT       20
#define KEY_CTRLU       21
#define KEY_CTRLV       22
#define KEY_CTRLW       23
#define KEY_CTRLX       24
#define KEY_CTRLY       25
#define KEY_CTRLZ       26
#define KEY_TAB         9
#define KEY_SPACE       32
#define KEY_BKSPC       127



/* bosh action struct */
typedef struct bosh_action_t {
  int   dest;
  char *command;
  char *prompt;
} ACTION;


/* bosh structure, defines a pane in the bosh window. */
typedef struct {

  /* process */
  char *command;
  char *common;
  int exit;

  int childin;
  int childout;
  int childerr;
  pid_t childpid;
  char tmpfscript[18]; /* made with mkstmp so always fixed length */
  char tmpfpipe[18];   /* " */

  /* interface */
  LIST *buf;
  int   width;
  int   offset;
  int   cursor;
  int   line;
  char *pipe;
  char *title;

  /* options */
  int    autorefresh;
  int    cursorsize;
  int    cursormovement;
  char  *multilineseperator;
  char  *preaction;
  int    refresh;
  int    searchwrap;
  int    header;
  int    footer;

  /* actions */
  ACTION action[37];

} bosh_t;


bosh_t *bosh_t_new();
int bosh_t_free(bosh_t *b);



//extern bosh_t *bosh;          /* pointer to current BOSH struct (only 1 ever used at the moment!) */
extern LIST   *boshlist;      /* linked list for BOSH structs */
extern int     boshlistpos;
extern char   *conf;
extern char   *confpath;
extern char   *shell;

extern int     ULINES;        /* usable lines */
extern char   *REPLY;         /* input return */
extern char   *search;

extern char   *BOSH;
extern char   *BOSHPARAM;
extern char    BOSHVARFILE[18];
extern char   *BOSHERR;
extern int     boshuservars;
extern char  **boshuservar;

stray_t       *childarg;


void bosh_init();
void bosh_finish(int s);
int  bosh_mainloop(int mode);
int  bosh_redraw();

int readstr(char *prompt);
int readstrp(char *prompt, char *previous);

#endif
