/*
Misc functions

$Id: misc.c,v 1.12 2009/03/21 17:44:22 alexsisson Exp $

(C) Copyright 2005-2009 Alex Sisson (alexsisson@gmail.com)

This file is part of bosh.

bosh is free software; you can redistribute it and/or modify
under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

bosh is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with bosh; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <sys/file.h>
#include <regex.h>

#include "misc.h"
#include "stray.h"

char *strdup2(const char *s1, const char *s2) {
  char *r;
  r = malloc(strlen(s1)+strlen(s2)+1);
  if(r) {
    sprintf(r,"%s%s",s1,s2);
  }
  return r;
}

/* makes a \0 terminated string of length len filled with c */
char *strmak(size_t len, int c) {
  static char *buf = NULL;
  free(buf);
  buf = malloc(len+1);
  if(buf) {
    memset(buf,c,len);
    buf[len] = 0;
  }
  return buf;
}

char *strswp(char *s, int c, int replace) {
  char *p = strchr(s,c);
  if(p)
    *p = replace;
  return p;
}


/* creates optstring arg (short opts) from option structure */
char *getopt_mkoptstring(struct option *longopts)
{
  int n = 0;
  char *optstring,*p;
  struct option *o = longopts;

  while(o->name || o->has_arg || o->flag || o->val) {
    n += 1 + o->has_arg;
    if(o->flag)
      return NULL; /* fail because not returning o->val */
    o++;
  }

  p = optstring = malloc(n+1);
  o = longopts;

  while(o->name || o->has_arg || o->flag || o->val) {
    *p++ = o->val;
    n = o->has_arg;
    while(n--)
      *p++ = ':';
    o++;
  }
  *p = 0;

  return optstring;
}


/* fatal error message and exit */
void bosh_fatal_err(int code, char *format, ...) {
  va_list v;
#ifdef LOG
  char s[512];
  va_start(v,format);
  vsprintf(s,format,v);
  va_end(v);
  fprintf(stderr,"%s: %s\n",PACKAGE,s);
  bosh_log("fatal: %s\n",s);
#else
  va_start(v,format);
  vfprintf(stderr,format,v);
  va_end(v);
#endif
  exit(code);
}

/* fprintf which also logs */
void bosh_fprintfl(FILE *stream, char *logprefix, const char *format, ...) {
  va_list v;
#ifdef LOG
  char s[512];
  va_start(v,format);
  vsprintf(s,format,v);
  va_end(v);
  fprintf(stream,"%s",s);
  bosh_log("%s %s",logprefix,s);
#else
  va_start(v,format);
  vfprintf(stream,format,v);
  va_end(v);
#endif
}


/* logging */
#ifdef LOG
FILE *BOSHLOG = NULL;

void bosh_log_open(const char *mode) {
  BOSHLOG = fopen("bosh.log",mode);
}

void bosh_log(char *format, ...) {
  va_list v;
  if(BOSHLOG) {
    flock(fileno(BOSHLOG),LOCK_EX);
    fseek(BOSHLOG,0,SEEK_END);
    va_start(v,format);
    vfprintf(BOSHLOG,format,v);
    va_end(v);
    fflush(BOSHLOG);
    flock(fileno(BOSHLOG),LOCK_UN);
  }
}

void bosh_log_stray(stray_t *a) {
  if(BOSHLOG) {
    flock(fileno(BOSHLOG),LOCK_EX);
    fseek(BOSHLOG,0,SEEK_END);
    stray_debug(a,BOSHLOG);
    fflush(BOSHLOG);
    flock(fileno(BOSHLOG),LOCK_UN);
  }
}

#endif

/* regex */
regex_t *bosh_regex = NULL;

int bosh_regex_init(char *s) {
  free(bosh_regex);
  bosh_regex = malloc(sizeof(regex_t));
  return regcomp(bosh_regex,s,REG_EXTENDED|REG_ICASE|REG_NOSUB);
}

int bosh_regex_try(char *s) {
  return regexec(bosh_regex,s,0,0,0);
}

