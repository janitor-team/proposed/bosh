/*
Config file related routines.

$Id: rc.h,v 1.14 2009/03/20 15:48:52 alexsisson Exp $

(C) Copyright 2002-2009 Alex Sisson (alexsisson@gmail.com)

This file is part of bosh.

bosh is free software; you can redistribute it and/or modify
under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

bosh is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with bosh; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/


#ifndef RC_H_INCLUDED
#define RC_H_INCLUDED

#include "bosh.h"
#include "stray.h"

int bosh_rc_read( char *name, bosh_t *bosh, stray_t *arg);
int bosh_rc_write(char *name, bosh_t *bosh);
int bosh_parseargs(bosh_t *bosh, stray_t *arg);
int bosh_action_set(bosh_t *bosh, char *a, char *v);

#endif
